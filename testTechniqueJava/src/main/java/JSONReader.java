
import java.io.FileReader;
import java.util.Iterator;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

public class JSONReader {
    public JSONReader() {
    }
    public void json(String file, String prof) {
        try {
            JSONParser jsonParser = new JSONParser();
            Object object = jsonParser.parse(new FileReader(file));
            JSONArray jsonArray = (JSONArray) object;
            Iterator iterator = jsonArray.iterator();
            Long TotalSalaire = 0l;
            double moyenneSalary = 0;
            int nombreSalarier = 0;
            boolean testProf = false;
            while (iterator.hasNext()) {
                JSONObject jsonObject = (JSONObject) iterator.next();
                Long salaire = (Long) jsonObject.get("salaire");
                String profession = (String) jsonObject.get("profession");
                if (profession.equalsIgnoreCase(prof)) {
                    nombreSalarier++;
                    TotalSalaire += salaire;
                    testProf = true;
                }
            }
            if (testProf) {
                moyenneSalary = (TotalSalaire / nombreSalarier);
                System.out.println("Le nombre total de salarier(s) " + prof + " est " + nombreSalarier + ", le salaire total est "
                        + TotalSalaire + "K£ et la moyenne est " + moyenneSalary + "K£");
            } else {
                System.out.println("La profession renseignée n'existe pas dans votre fichier..!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
