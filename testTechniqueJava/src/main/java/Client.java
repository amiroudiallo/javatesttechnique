/**
 * Cette classe permet la gestion des clients.
 */
public class Client {
    /**
     * représente le nom du client.
     */
    private String nomClient;
    /**
     * représente le prénom du client.
     */
    private String prenomClient;
    /**
     * représente l'âge du client.
     */
    private Integer ageClient;
    /**
     * représente la profession du client.
     */
    private String professionClient;
    /**
     * représente le salaire du client.
     */
    private Double salaireClient;

    /**
     * constructeur.
     * @param nomClient nom du client
     * @param prenomClient prénom du client
     * @param ageClient âge du client
     * @param professionClient profession du client
     * @param salaireClient salaire du client
     */
    public Client(String nomClient, String prenomClient, Integer ageClient, String professionClient,
                  Double salaireClient) {
        this.nomClient = nomClient;
        this.prenomClient = prenomClient;
        this.ageClient = ageClient;
        this.professionClient = professionClient;
        this.salaireClient = salaireClient;
    }

    /**
     * getter pour nom client
     * @return string
     */
    public String getNomClient() {
        return nomClient;
    }

    /**
     * setter nom client
     * @param nomClient nom du client
     */
    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }

    /**
     * getter prénom du client.
     * @return prénom du client
     */
    public String getPrenomClient() {
        return prenomClient;
    }

    /**
     * setter prénom du client.
     * @param prenomClient prénom du client
     */
    public void setPrenomClient(String prenomClient) {
        this.prenomClient = prenomClient;
    }

    /**
     * getter age du client.
     * @return int
     */
    public Integer getAgeClient() {
        return ageClient;
    }

    /**
     * setter age du client.
     * @param ageClient int
     */
    public void setAgeClient(Integer ageClient) {
        this.ageClient = ageClient;
    }

    /**
     * getter profession du client.
     * @return string
     */
    public String getProfessionClient() {
        return professionClient;
    }

    /**
     * setter profession du client
     * @param professionClient profession
     */
    public void setProfessionClient(String professionClient) {
        this.professionClient = professionClient;
    }

    /**
     * getter salaire du client.
     * @return double
     */
    public Double getSalaireClient() {
        return salaireClient;
    }

    /**
     * setter salaire du client.
     * @param salaireClient salaire du client
     */
    public void setSalaireClient(Double salaireClient) {
        this.salaireClient = salaireClient;
    }

    /**
     * methode toString
     * @return object
     */
    @Override
    public String toString() {
        return "Client ["
                + "  nomClient='" + nomClient
                + ", prenomClient='" + prenomClient
                + ", ageClient=" + ageClient
                + ", professionClient='" + professionClient
                + ", salaireClient=" + salaireClient
                + ']';
    }
}
