import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XMLReader {
    public XMLReader() {
    }

    public void xml(String file, String profession) throws IOException, ParserConfigurationException, SAXException {

        int nombreSalarier = 0;
        double totalSalaire = 0;
        double moyenneSalary = 0;
        File fichier = new File(file);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document document = dBuilder.parse(fichier);
        document.getDocumentElement().normalize();
        List<Client> clientList = new ArrayList<>();
        NodeList nodeList = document.getDocumentElement().getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                String firstName = element.getElementsByTagName("firstname").item(0).getChildNodes().item(0).getNodeValue();
                String lastName = element.getElementsByTagName("lastname").item(0).getChildNodes().item(0).getNodeValue();
                Integer age = Integer.parseInt(element.getElementsByTagName("age").item(0).getChildNodes().item(0).getNodeValue());
                String prof = element.getElementsByTagName("profession").item(0).getChildNodes().item(0).getNodeValue();
                Double salary = Double.parseDouble(element.getElementsByTagName("salary").item(0).getChildNodes().item(0).getNodeValue());
                clientList.add(new Client(firstName, lastName, age, prof, salary));
            }
        }
        boolean testProf = false;
        for (Client client: clientList) {
            double salaire = client.getSalaireClient();
            if (client.getProfessionClient().equalsIgnoreCase(profession)) {
                nombreSalarier++;
                totalSalaire += salaire;
                testProf = true;
            }
        }
        if (testProf) {
            moyenneSalary = (totalSalaire / nombreSalarier);
            System.out.println("Le nombre total de salarier(s) " + profession + " est " + nombreSalarier
                    + ", le salaire total est " + totalSalaire + "K£ et la moyenne est " + moyenneSalary + "K£");
        } else {
            System.out.println("La profession renseignée n'existe pas dans votre fichier..!");
        }
        /*clientList.stream().peek(client -> {
            System.out.println(client);
        }).collect(Collectors.toList());*/

    }
}
