import java.io.FileReader;
import java.util.Scanner;

public class CSVReader {

    public CSVReader() {
    }
    public void csv(String file, String prof) throws Exception {
        Scanner input = new Scanner(new FileReader(file));
        input.useDelimiter(",");
        int nombreSalarier = 0;
        String line = "";
        double TotalSalaire = 0;
        double moyenneSalary = 0;
        boolean testProf = false;
        try {
            while (input.hasNext()) {
                line = input.nextLine();
                double salaire = Integer.parseInt(line.split(",")[4]);
                String profession = line.split(",")[3];
                if (profession.equalsIgnoreCase(prof)) {
                    TotalSalaire += salaire;
                    nombreSalarier++;
                    testProf = true;
                }
            }
            if (testProf) {
                moyenneSalary = (TotalSalaire / nombreSalarier);
                System.out.println("Le nombre total de salarier(s) " + prof + " est " + nombreSalarier
                        + ", le salaire total est " + TotalSalaire + "K£ et la moyenne est " + moyenneSalary + "K£");
            } else {
                System.out.println("La profession renseignée n'existe pas dans votre fichier..!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
