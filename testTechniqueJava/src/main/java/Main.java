import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        try {
            String reponse = null;
            Integer finalResponse = null;
            do {
                Scanner input = new Scanner(System.in);
                System.out.println("+++++ BONJOUR BIENVENUE SUR LE PROGRAMME DE CLIMAX +++++");
                System.out.println("************** MENU ******************");
                System.out.println("\t1- Fichier de type CSV");
                System.out.println("\t2- Fichier de type JSON");
                System.out.println("\t3- Fichier de type XML");
                System.out.println("\t0- Pour terminer le programme");
                System.out.println("**************************************");
                System.out.print("Veuillez entrer le numéro de l'opération dont vous souhaitez effectuer:\n");
                Integer fileType = input.nextInt();
                switch (fileType) {
                    case 1:
                        System.out.println("Vous avez choisi le type CSV!");
                        System.out.print("Veuillez entrer la profession: ");
                        Scanner saisie = new Scanner(System.in);
                        String prof = saisie.nextLine();
                        CSVReader csvreader = new CSVReader();
                        String fileName = "C:\\Users\\User\\Desktop\\testTechniqueJava\\testTechniqueJava\\" +
                                "src\\main\\resources\\file\\csv-file.csv";
                        csvreader.csv(fileName, prof);
                        System.out.println("Voulez-vous continuez l'opération (O/N) ?:");
                        reponse = saisie.nextLine();
                        break;
                    case 2:
                        System.out.println("Vous avez choisi le type JSON!");
                        System.out.print("Veuillez entrer la profession: ");
                        Scanner jsonSaisi = new Scanner(System.in);
                        String profs = jsonSaisi.nextLine();
                        System.out.println(fileType + " " + profs);
                        JSONReader jsonReader = new JSONReader();
                        String jsonFile = "C:\\Users\\User\\Desktop\\testTechniqueJava\\testTechniqueJava\\src\\main" +
                                "\\resources\\file\\json-file.json";
                        jsonReader.json(jsonFile, profs);
                        System.out.println("Voulez-vous continuez l'opération (O/N) ?:");
                        reponse = jsonSaisi.nextLine();
                        break;
                    case 3:
                        System.out.println("Vous avez choisi le type XML!");
                        System.out.print("Veuillez entrer la profession: ");
                        Scanner xmlCase = new Scanner(System.in);
                        String profes = xmlCase.nextLine();
                        XMLReader xmlReader = new XMLReader();
                        String xmlFile = "C:\\Users\\User\\Desktop\\testTechniqueJava\\testTechniqueJava\\src\\main" +
                                "\\resources\\file\\xml-file.xml";
                        xmlReader.xml(xmlFile, profes);
                        System.out.println("Voulez-vous continuez l'opération (O/N) ?:");
                        reponse = xmlCase.nextLine();
                        break;
                    default:
                        System.out.println("Vérifiez le numéro saisi..!");
                }
            } while (reponse.equalsIgnoreCase("O"));
        } catch (Exception e) {
            e.getMessage();
        }


    }
}
